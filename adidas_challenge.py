import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


class TestAdidas(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        chrome_options = Options()
        chrome_options.add_argument('--start-fullscreen')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.driver.get('http://store.demoqa.com/')
        self.commentSent = 'comment filled' #modify comment

    def test_fail_mail(self):
        self.driver.find_element_by_link_text('Sample Page').click()
        inputElement = self.driver.find_element_by_id('comment')
        inputElement.send_keys(self.commentSent)
        inputElement = self.driver.find_element_by_id('author')
        inputElement.send_keys('Author filled')
        inputElement = self.driver.find_element_by_id('email')
        inputElement.send_keys('notemail')
        self.driver.find_element_by_id('submit').click()
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, 'error-page'))
            )
        except: print 'Error page not displayed'
        self.driver.find_element_by_partial_link_text('Back').click()
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, 'commentform'))
            )
        except: print 'Back to sample page failed'


    def test_right_mail(self):
        inputElement = self.driver.find_element_by_id('email')
        inputElement.send_keys('test@mail.com')
        self.driver.find_element_by_id('submit').click()
        try:
            element_class = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME,'comment-body'))
            )
        except: print 'There is no comments registered'
        comments = self.driver.find_elements_by_css_selector('.comment-body p')
        inserted = False
        for i in comments:
            if ((i.text).encode('utf8')) == str(self.commentSent):
                inserted = True        
        self.assertTrue(inserted)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

