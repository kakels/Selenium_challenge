import unittest
import HtmlTestRunner
import os
from adidas_challenge import TestAdidas


test_mail = unittest.TestLoader().loadTestsFromTestCase(TestAdidas)


test_suite = unittest.TestSuite([test_mail])

# configure HTMLTestRunner options
runner = HtmlTestRunner.HTMLTestRunner(output='report')


runner.run(test_suite)